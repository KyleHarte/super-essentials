package me.Sparkio.ServerEssentials.Commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.Sparkio.ServerEssentials.Main;
import me.Sparkio.ServerEssentials.Managers.ChatManager;

public class EmergencyOP implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(label.equalsIgnoreCase("emergencyop") && sender.hasPermission("emergency.op")) {
			if(args.length == 1){
				//emergencyop Args[0 - set]
				if(args[0].equals("set")){
					Main.getMain().getConfig().set("EmergencyOPPassword", args[0].toString());
					sender.sendMessage(ChatManager.getChatManager().getPrefix() + ChatColor.WHITE +  " EmergencyOP Password has been set to " + args[0].toString() + " " + ChatManager.getChatManager().getPrefix());
				} else if(args[0].equals(Main.getMain().getConfig().get("EmergencyOPPassword"))){
					if(sender instanceof Player){
						Player player = (Player) sender;
						player.setOp(true);
					} else {
						sender.sendMessage(ChatManager.getChatManager().getPrefix() + " You Cannot OP the Console "+ ChatManager.getChatManager().getPrefix());
					}
				} else {
					sender.sendMessage(ChatManager.getChatManager().getPrefix() + " Issue with your Syntax /emergencyop set [NEW PASSWORD] Or /emergencyop [Your OP Password] "+ ChatManager.getChatManager().getPrefix());
				}
			}
		}
		return false;
	}
	

}
