package me.Sparkio.ServerEssentials.Managers;

import me.Sparkio.ServerEssentials.Main;

public class ChatManager {
	 private static ChatManager instance;

	    public ChatManager() {
	        instance = this;
	    }
  public static ChatManager getChatManager(){
	  return instance;
  }
  
  public String getPrefix(){
	  return Main.getMain().getConfig().getString("prefix");
  }
  public String NoPermMessage(){
	  return Main.getMain().getConfig().getString("NoPermMessage");
  }
}
