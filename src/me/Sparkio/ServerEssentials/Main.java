package me.Sparkio.ServerEssentials;



import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;


public class Main extends JavaPlugin {
	
	 private static Main instance;

	    public Main() {
	        instance = this;
	    }
	    public static Main getMain(){
	    	return instance;
}
	 
	@Override
	public void onEnable() {
		getConfig().options().copyDefaults(true);
		saveConfig();
		System.out.println(ChatColor.RED + "[NOTICE]" + ChatColor.WHITE + "Server Essentials has been Enabled Successfully" + ChatColor.RED + "[NOTICE]");
	}
	
	@Override
	public void onDisable() {
		saveConfig();
		System.out.println(ChatColor.RED + "[NOTICE]" + ChatColor.WHITE + "Server Essentials has been Disabled Successfully" + ChatColor.RED + "[NOTICE]");
	}
	
	public void registerCommands(){
		
		
	}
}
